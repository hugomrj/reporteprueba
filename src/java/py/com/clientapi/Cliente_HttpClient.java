/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.clientapi;


import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hugo
 */
public class Cliente_HttpClient {

    
        private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    
    
    public static void main(String[] args) throws IOException, InterruptedException {
        
        
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://demo.javete.net/webapp/api/sucursales/all"))
                //.setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
                .build();

        
        HttpResponse<String> response = 
                httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        // print response headers
        //HttpHeaders headers = response.headers();
        HttpHeaders headers = response.headers();
        
            //headers.map().forEach((k, v) -> System.out.println(k + ":" + v));
            Map<String, List<String>> nn = headers.map();
            
            for (Map.Entry<String, List<String>> entry : nn.entrySet()) {
            Object key = entry.getKey();
            Object val = entry.getValue();
            
            System.out.println(key + " - " + val);
            
        }
        
        // print status code
        System.out.println(response.statusCode());

        // print response body
        System.out.println(response.body());
                


    }    
    
 
    
}



    
    
    
    