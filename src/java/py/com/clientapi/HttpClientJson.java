/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.clientapi;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

/**
 *
 * @author hugo
 */
public class HttpClientJson {
    
    
    public HttpResponse<String> syncGet( String url ) 
            throws IOException, InterruptedException{
    
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
              .uri(URI.create( url ))
            //.setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
              .build();

        
        HttpResponse<String> response =
              client.send(request, BodyHandlers.ofString());
                
        /*
            HttpHeaders headers = response.headers();
        
            Map<String, List<String>> nn = headers.map();
            
            for (Map.Entry<String, List<String>> entry : nn.entrySet()) {
                Object key = entry.getKey();
                Object val = entry.getValue();            
                System.out.println(key + " - " + val);
            }
            System.out.println(response.body());    
        */
            
        return response;
    }
    
}
