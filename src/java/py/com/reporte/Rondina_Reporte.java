/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package py.com.reporte;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.http.HttpResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.query.JsonQueryExecuterFactory;
import py.com.clientapi.HttpClientJson;

/**
 *
 * @author hugo
 */

@WebServlet(name = "Rondina_Reporte", 
        urlPatterns = {"/rondina_reporte.pdf"})


public class Rondina_Reporte extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, InterruptedException {

        
            HttpSession sesion = request.getSession();        
        
        try { 
        
                    
                    
                    String archivo = "rondina01";            
                    String archivo_jrxml = archivo+".jrxml";
                    response.setHeader("Content-disposition","inline; filename="+archivo+".pdf");
                    response.setContentType("application/pdf");


                    String url =  request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";
                    url = url + archivo_jrxml;

                    // parametros
                    Map<String, Object> parameters = new HashMap<String, Object>();

String report_path = request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";;
report_path = report_path.replace("\\", "/") ;






String urlws = "http://181.94.221.43:8212/ords/inv/consultas/clientes?codEmpresa=1&pass=rondi2021!";
String reportContents = "" ;

HttpClientJson httpclient = new HttpClientJson();
HttpResponse<String> httpResponse = httpclient.syncGet(urlws);

reportContents = httpResponse.body().toString();


InputStream is = new ByteArrayInputStream(reportContents.getBytes());

parameters.put(JsonQueryExecuterFactory.JSON_INPUT_STREAM, is);
//JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params);




                    
                    //Convercion conversion = new Convercion();            
                    //parameters.put("par_monto_letras", conversion.numeroaLetras(monto_total));

                    JasperReport report = JasperCompileManager.compileReport(url);            
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters);
                    

                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    byte[] reportePdf = JasperExportManager.exportReportToPdf(jasperPrint);

                    response.setContentLength(reportePdf.length);

                    servletOutputStream.write(reportePdf, 0, reportePdf.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();    

                    
                    response.setStatus(HttpServletResponse.SC_ACCEPTED);

                    
                    
                    
                    
                
//                autorizacion.actualizar();

            
        }         
        catch (JRException ex) 
        {
            Logger.getLogger(Rondina_Reporte.class.getName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        finally
        {          
            sesion.invalidate();
        }

            
            
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Rondina_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Rondina_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
